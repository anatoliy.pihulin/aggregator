<?php

namespace app\controllers;

use app\models\Postback;
use yii\filters\AccessControl;
use yii\web\Controller;

class PostbackController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'only' => ['grid-postback'],
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['grid-postback'],
                        'roles' => ['@'],
                    ],

                ],
            ],
        ];
    }

    public function actionPostback($cid, $campaign_id, $time, $event)
    {
        if ((new Postback)->newRecord($cid, $campaign_id, $time, $event)) {
            $messege = 'nice';
        } else {
            $messege = 'not nice';
        };

        return $this->render('postback', ['result' => $messege]);
    }

    public function actionGridPostback($startDate = null, $endDate = null) {
        if (empty($startDate) && empty($endDate)) {
            $endDate = $startDate = date('Y-m-d');
        }
        $postback = (new Postback)->searchGroupCid($startDate, $endDate);
        return $this->render('grid-postback',['postbackGrid' => $postback]);
    }
}
