<?php
namespace app\modules\api\controllers;

use app\models\Postback;
use yii\web\Controller;

class ApiController extends Controller
{
    public function actionIndex($startDate = null, $endDate = null)
    {
        return $this->asJson(Postback::getAll($startDate, $endDate));
    }

    /**
     * @inheritdoc
     */
    protected function verbs()
    {
        return [
            'index' => ['GET', 'HEAD'],
        ];
    }
}