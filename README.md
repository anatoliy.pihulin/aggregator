
Начальная настройка
-------------------
В папке 
`/config ` создать файл `db.php`

Пример файла `db.php`

~~~
<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=dbname',
    'username' => '',
    'password' => '',
    'charset' => 'utf8',

    // Schema cache options (for production environment)
    //'enableSchemaCache' => true,
    //'schemaCacheDuration' => 60,
    //'schemaCache' => 'cache',
];


~~~

Применяем миграции командой:
~~~
yii migrate
~~~

Начало работы
-------------------

По умолчанию у нас один пользователь:

username: `admin`
password: `admin`