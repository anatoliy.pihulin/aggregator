function changeDate()
{
    var current_datetime = new Date()
    var day = current_datetime.getDate()
    var month = current_datetime.getMonth()+1
    var year = current_datetime.getFullYear()
    var hours = current_datetime.getHours()
    var minutes = current_datetime.getMinutes()
    var seconds = current_datetime.getSeconds()

    return day+"."+month+"."+year+" "+hours+":"+minutes+":"+seconds;
}

function getServerDateTime() {
    $.ajax({
        url: window.location.origin + '/site/get-server-time',
        type: "POST",
        success: function (date) { console.log(date) }
    });
}

setInterval(function () {
    document.getElementById('current_date_time_block').innerHTML = changeDate();
}, 1000);