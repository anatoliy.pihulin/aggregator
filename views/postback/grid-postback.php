<?php

use app\models\Postback;
use kartik\form\ActiveForm;
use yii\bootstrap4\Html;
use yii\data\ArrayDataProvider;
use kartik\grid\GridView;
use kartik\date\DatePicker;
use yii\helpers\Url;
use yii\web\View;

$this->registerJsFile('@web/js/calculate_grid_statistics.js', ['position' => View::POS_END ]);

$form = ActiveForm::begin([
    'id' => 'login-form-vertical',
    'type' => ActiveForm::TYPE_VERTICAL
]);

$form = ActiveForm::begin([
    'options' => ['data' => ['pjax' => true]],
    'action' => Url::to(['/postback/grid-postback']),
    'method' => 'get'
    // остальные опции ActiveForm
]);

echo DatePicker::widget([
    'name' => 'startDate',
    'value' => empty($_GET['startDate'])? date('d-m-yy') : $_GET['startDate'],
    'type' => DatePicker::TYPE_RANGE,
    'name2' => 'endDate',
    'value2' => empty($_GET['endDate'])? date('d-m-yy'): $_GET['endDate'],
    'pluginOptions' => [
        'autoclose' => true,
        'format' => 'dd-mm-yyyy'
    ],
    'language' => 'ru'
]);

echo Html::tag('br');

echo Html::submitButton('Поиск',['class' => 'btn btn-primary btn-lg btn-block']);

ActiveForm::end();

echo Html::tag('br');

echo GridView::widget([
    'dataProvider' => new ArrayDataProvider([
        'allModels' => $postbackGrid,
        'pagination' => false
    ]),
    'showPageSummary' => true,
    'layout'=>"{sorter}\n{pager}\n{summary}\n{items}",
    'columns' => [
        [
            'format' => 'text',
            'label' => 'ID',
            'value' => function($model, $key, $index, $column){
                if ($model['campaign_id'] == 1) {
                    return 'organic';
                }
                return $model['campaign_id'];
            },
            'pageSummary'=>'Сумма',
            'pageSummaryOptions' => ['colspan' => 1],
        ],
        [
            'attribute' => 'click',
            'pageSummary' => true,
            'pageSummaryOptions' => [
                'class' => 'kv-page-summary warning',
                'id' => 'click_count'
            ]
        ],
        [
            'attribute' => 'install',
            'pageSummary' => true,
            'pageSummaryOptions' => [
                'class' => 'kv-page-summary warning',
                'id' => 'install_count'
            ]
        ],
        [
            'format' => ['text',2],
            'label' => 'CRi, %',
            'value' => function($model, $key, $index, $column){
                return Postback::getCri($model['install'], $model['click']);
            },
            'pageSummaryOptions' => [
                'class' => 'kv-page-summary warning',
                'id' => 'cri_count'
            ]
        ],
        [
            'attribute' => 'trial',
            'pageSummary' => true,
            'pageSummaryOptions' => [
                'class' => 'kv-page-summary warning',
                'id' => 'trial_count'
            ]
        ],
        [
            'format' => 'text',
            'label' => 'CRti, %',
            'value' => function($model, $key, $index, $column){
                return Postback::getCRti($model['trial'], $model['install']);
            },
            'pageSummaryOptions' => [
                'class' => 'kv-page-summary warning',
                'id' => 'crti_count'
            ]
        ],
        [
            'attribute' => 'subscr',
            'pageSummary' => true,
            'pageSummaryOptions' => [
                'class' => 'kv-page-summary warning',
                'id' => 'subscr_count'
            ]
        ],
        [
            'format' => ['text',2],
            'label' => 'CRs,%',
            'value' => function($model, $key, $index, $column){
                return Postback::getCRs($model['subscr'], $model['install']);
            },
            'pageSummaryOptions' => [
                'class' => 'kv-page-summary warning',
                'id' => 'crs_count'
            ]
        ]
    ],
]);

?>