<?php

use app\models\Postback;
use yii\db\Migration;

/**
 * Handles the creation of table `{{%new_postback}}`.
 */
class m201125_085147_create_new_postback_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%postback}}', [
            'id' => $this->primaryKey(),
            'cid' => $this->string()->null(),
            'campaign_id' => $this->integer()->notNull(),
            'time' => $this->bigInteger()->notNull(),
            'event' => $this->string()->null(),
            'created_at' => $this->dateTime(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%postback}}');
    }
}
