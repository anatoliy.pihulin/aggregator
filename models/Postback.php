<?php

namespace app\models;

use DateTime;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;

/**
 * This is the model class for table "postback".
 *
 * @property int $id
 * @property string|null $cid
 * @property int $campaign_id
 * @property int $time
 * @property string|null $event
 */
class Postback extends \yii\db\ActiveRecord
{
    const ORGANIC_EVENT_CID = 'organic';
    const ORGANIC_EVENT_CAMPAIGN_ID = 1;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'postback';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['campaign_id', 'time'], 'required'],
            [['campaign_id', 'time'], 'integer'],
            [['cid', 'event'], 'string', 'max' => 255],
            [['created_at'], 'datetime', 'format' => 'php:Y-m-d H:i:s']
            //['cid', 'unique', 'when' => function($model) {
            //    return $model->event == 'click';
            //}],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cid' => 'Cid',
            'campaign_id' => 'Campaign ID',
            'time' => 'Time',
            'event' => 'Event',
        ];
    }

    public function newRecord($cid, $campaign_id, $time, $event) {

        if(empty($cid) && empty($campaign_id)) {
            $cid = self::ORGANIC_EVENT_CID;
            $campaign_id  = self::ORGANIC_EVENT_CAMPAIGN_ID;
        }

        $value = [
            'cid' => $cid,
            'campaign_id' => $campaign_id,
            'time' => $this->checkMillisecondsInt($time)? $this->convertMillisecondsInSeconds($time): $time,
            'event' => $event,
            'created_at' => date('Y-m-d H:i:s')
        ];

        $record = new $this;

        $record->attributes = $value;

        return $record->save() ? true : false;
    }

    public function searchGroupCid($startDate = null, $endFate = null) {
        $search = Yii::$app->db->createCommand('select P.cid, P.campaign_id, P.time, P.event,
    SUM(CASE WHEN P.event = "click" THEN 1 ELSE 0 END) AS click,
    SUM(CASE WHEN P.event = "install" THEN 1 ELSE 0 END) AS install,
	SUM(CASE WHEN P.event = "trial_started" THEN 1 ELSE 0 END) AS trial,
	SUM(CASE WHEN P.event = "trial_converted" THEN 1 ELSE 0 END) AS subscr
    from postback P 
    where P.time >= :timeStart AND P.time <= :timeEnd
    group by P.campaign_id')->bindValue(':timeStart', $this->getStartDate($startDate))->bindValue(':timeEnd', $this->getEndDate($endFate))->queryAll();

        return $search;
    }

    public function getStartDate($startDate) {
        $date = new DateTime($startDate);
        return strtotime($date->format('Y-m-d 00:00:00'));
    }
    public function getEndDate($endDate) {
        $date = new DateTime($endDate);
        return strtotime($date->format('Y-m-d 23:59:59'));
    }

    public function convertMillisecondsInSeconds($milliseconds) {
        return round(($milliseconds/1000), 0);
    }

    public function checkMillisecondsInt($int) {
        return (strlen($int) > 10) ? true : false;
    }

    public function getCri($install, $click) {
        return ($click == 0) ? 0 : round(($install/$click) * 100, 2);
    }

    public function getCRti($trial, $install) {
        return ($install == 0) ? 0 : round(($trial/$install) * 100, 2);

    }

    public function getCRs($subscr, $install) {
        return ($install == 0) ? 0 : round(($subscr/$install) * 100, 2);

    }

    public static function getAll($startDate = null, $endFate = null)
    {
        $result = [];


        $postbacks = Postback::searchGroupCid($startDate, $endFate);
        foreach ($postbacks as $key => $postback) {

            $result[$key] = [
                'id' => $postback['campaign_id'],
                'click' => $postback['click'],
                'install' => $postback['install'],
                'CRi' => Postback::getCri($postback['trial'],$postback['install']),
                'trial' => $postback['trial'],
                'CRti' => Postback::getCRti($postback['trial'],$postback['install'])
            ];
        }

        return $result;
    }

    public static function getTotal($provider, $columnName)
    {
        $total = 0;
        foreach ($provider as $item) {
            $total += $item[$columnName];
        }
        return $total;
    }
}
